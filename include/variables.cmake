# compiler.cmake
#
# Copyright (c) 2019-2021 - BladeRunner Labs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

if (NOT _VARIABLES_CMAKE_INCLUDED)

macro(set_parent_var var_name var_value)
    get_directory_property(_HAS_PARENT PARENT_DIRECTORY)
    if (_HAS_PARENT)
        set(${var_name} ${var_value} PARENT_SCOPE)
        #message("${var_name} = ${var_value} PARENT_SCOPE")
    else()
        set(${var_name} ${var_value})
        #message("${var_name} = ${var_value}")
    endif()
endmacro()

macro(set_global_var var_name var_value)
    set_property(GLOBAL PROPERTY ${var_name} ${${var_value}})
endmacro()

macro(get_global_var var_name local_var)
    get_property(${local_var} GLOBAL PROPERTY ${var_name})
endmacro()

set_parent_var(_VARIABLES_CMAKE_INCLUDED YES)

endif()
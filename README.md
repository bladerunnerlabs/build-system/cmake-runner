# `cmake-runner` - a versatile CMake framework
`cmake-runner` is a templated, macros-based `CMake` framework. It is intended for defining `CMake` projects easily, predictably and uniformly.

[[_TOC_]]

# Overview

## About CMake

### What is CMake
`CMake` is a meta-language for defining build environements. It generates the actual makefiles according to the platform requirements. 

`CMake` has gained substantial popularity for being:

- Readable (e.g. compared to `make`)
- Cross-platform and compiler-independent
- Easy to start : works “out of the box”
- Supported by IDEs

### CMake evolution
`CMake` has gradually evolved into a rich & extensible language. 

The notion *modern CMake* usually implies the newer, object-oriented style semantics.

Under this paradigm *everything is a target*: libraries, executables, APIs. The targets store all file lists, attributes (symbols, options, dependencies *etc*) and directives required for building the final distributable unit.

Targets can be bundled into *packages*. Packages are built, installed and exported through special `CMake` configuration files which can be automatically generated. The export files describe binaries, headers and their properties as ready-made targets.

Projects import external packages through the associated export files and gain access to their targets, as if they were defined inside the project itelf. 

Packages export their *namespace* prefixes, so that the importing projects may use target names along with the prefixes to avoid name collisions and increase readability.

### Typical CMake problems
On practical level `CMake` has few problems:

- there are multiple options for almost any build system objective, so it's sometimes hard choose the right ones;
- it is possible to use different coding patterns and/or styles so mixing them in a single project may render it inconsistent;
- some tasks are implemented by using repetitive, almost 'boiler-plate' patterns of commands;
- to make the complex projects scale well one need to impose a well-defined structure, which is hard to maintain.

## About `cmake-runner`

### `cmake-runner` objectives
The idea of `cmake-runner` is to create a new CMake language extension:

- define a set of macros, to cover most of the usual build scenarios
- make the macros as simple as possible
- handle all repetative, obscure, unintuitive details there
- restrict usage to the new “dialect”, across the project
- support project modularization

### Packages Interaction
- every target pertains to a package
- each package instals its targets and exports them for further use
- the package namespace prefix (e.g. `alpha::`) is used uniformly
  - inside the package a namespaced target is resolved as an *alias* (e.g. `alpha::abc` = `abc`)
  - outside the package the target's prefix is a real namespace

### Single-tree and module-wise builds
It is possible to build each module project separately, so that each subsequent project uses the targets installed by the projects it depends upon. The right sequence of module builds must be choosen either manually or by an external orchestration tool.

Alternatively, all modules may be included as sub-projects to a root meta-project thus allowing them to be built in a common namespace and a single make execution environment. 

Module-by-module bulds may be conceptually simpler. Single-tree builds have few potential benefits:

- it speeds up complex builds by optimizing parallelizaton
- builds are initiated by a single command, sparing the dependency resolution and orchestration of module sub-projects

Both modes are automatically supported by `cmake-runner` [packages interaction](#packages_interaction).

# Using `cmake-runner`

`cmake-runner` may be used as a stand-alone module, so its git repo can be just cloned and used in a project, although it is best used as an infrastructure module maintained by [`build-runner`](https://gitlab.com/bladerunnerlabs/build-system/build-runner) in projects derived from [`project-template`](https://gitlab.com/bladerunnerlabs/build-system/project-template). These three modules are designed to work optimally together. 

## Required symbols

It is required to define the following `CMake` symbols:

- `BUILD_ROOT_DIR` - root of the build project; especially relevant for multi-module projects, like those maintained by [`build-runner`](https://gitlab.com/bladerunnerlabs/build-system/build-runner)
  - for [`build-runner`](https://gitlab.com/bladerunnerlabs/build-system/build-runner) projects the build root default is the project's parent directory
- `CMAKE_RUNNER_DIR` - location of `cmake-runner` module
  - for [`build-runner`](https://gitlab.com/bladerunnerlabs/build-system/build-runner) projects `cmake-runner` is located by default in `BUILD_ROOT_DIR`

These symbols must be defined either locally (in the project's root `CMakeLists.txt`) or through the command line (see [main `CMakeListss.txt` example](#example-of-a-project-cmakeliststxt-header).

Additional global options are:

- `INSTALL_DIR` - if defined, this symbol is translated directly to `CMAKE_INSTALL_PREFIX`, otherwise few defaults eventually determine the final `CMAKE_INSTALL_PREFIX` value
  - the default: `BUILD_ROOT_DIR/install`
  - may be overrided by `INSTALL_DIR` argument to [`define_package()`](#define_package)


## Including `cmake-runner` into a project

`cmake-runner` is implemented as a set of `CMake` macros and functions split into multiple include files according to their functionality and subjects. To use `cmake-runner` in a project one must include its macros first.

The basic functionality is available after including `cmake-runner-std.cmake` (which includes few other files) and this is a single file that should be enough for most of the projects.

Before including `cmake-runner` the symbol `CMAKE_RUNNER_DIR` must be defined, either locally or through the command line.

## ▶`cmake_runner_minimum_required()`
Requires a minimal verison of `cmake-runner` to start processing of CMake files.

Versions have a 3-component `major.minor.patch` format:

- **major version**: gross functionality and syntax changes; no backward compatibility is guaranteed
- **minor version**: some functionality and slight syntax changes; backward compatibility is guaranteed within the same **major** version
- **patch number**: fixes and slight functionality changes; no syntax changes relative to the parent **minor** version

### `cmake_runner_minimum_required()` macro format
```cmake
cmake_runner_minimum_required (
    VERSION <version-string>
)
```

## Example of a project `CMakeLists.txt` header

```cmake
cmake_minimum_required(VERSION 3.16)
project(my-project LANGUAGES CXX)

if (NOT BUILD_ROOT_DIR)
    set(BUILD_ROOT_DIR ${PROJECT_SOURCE_DIR}/..)
endif()

if (NOT CMAKE_RUNNER_DIR)
    set(CMAKE_RUNNER_DIR ${BUILD_ROOT_DIR}/cmake-runner)
endif()
set(INC_CMAKE_DIR ${CMAKE_RUNNER_DIR}/include)
include(${INC_CMAKE_DIR}/cmake-runner-std.cmake)

cmake_runner_minimum_required(1.4.0)
conan_install()
```

In this case symbols passed through the command line take precedence. The following command: 
```shell
# cmake -S my-project -B my-project/build -DCMAKE_RUNNER_DIR=/home/user/cmake-runner
``` 
will use `/home/user/cmake-runner` as `CMAKE_RUNNER_DIR`, while `BUILD_ROOT_DIR` will be set automatically to the project's parent dir.


# Package and import management macros

## ▶`conan_install()`
Install the [Conan](https://docs.conan.io/en/latest/introduction.html) packages declared in the project's [conanfile.txt](https://docs.conan.io/en/latest/reference/conanfile_txt.html).

### `conan_install()` macro format
```cmake
conan_install ()
```

## ▶`define_package()`

### `define_package()` macro format
```cmake
define_package ( <package-name>
    API_DIR <api-subdir-default>
    SRC_DIR <src-subdir-default>
    FILES <install-misc-files-list>
    INSTALL_DIR <install-prefix-dir>
    API_INSTALL_DIR <alt-api-dir>
    LIB_INSTALL_DIR <alt-lib-dir>
    BIN_INSTALL_DIR <alt-bin-dir>
)
```

### `define_package()` parameters

#### `API_DIR`
A user-defined default value for `API_DIR` argument of `cmake-runner` target-defining macros, e.g. [`define_lib(API_DIR)`](#api_dir-1).

This is a relative path from a target's hosting directory, which is supposed to contain the API (public) headers. The default is: `.` (the current directory).

#### `SRC_DIR`
A user-defined default value for `SRC_DIR` argument of `cmake-runner` target-defining macros, e.g. [`define_lib(SRC_DIR)`](#src_dir-1).

This is a relative path from a target's hosting directory, which is supposed to contain the source and private header files. The default is: `.` (the current directory).

#### `FILES`
A list of miscellaneous (confguration, docs, plain text) files to be unconditionally installed to the package install root directory.

#### `INSTALL_DIR`
Alternative package-specific install sub-directory for all miscellaneous (not API, LIB or BIN) files.

The root of installation for such files from all targets belonging to the package is always set to `${CMAKE_INSTALL_PREFIX}/<package-name>`.

This option allows to override the `<package-name>` part for a certain package.

#### `API_INSTALL_DIR`
Alternative package-specific path for *API* install subdirectory.

The default is `include`. This option allows to override it for a certain package.

#### `LIB_INSTALL_DIR`
Alternative package-specific path for *library* install subdirectory.

The default is `lib`. This option allows to override it for a certain package.

#### `BIN_INSTALL_DIR`
Alternative package-specific path for *binary* install subdirectory.

The default is `bin`. This option allows to override it for a certain package.

### `define_package()` example
```cmake
define_package ( module1
    API_DIR
        api
    INSTALL_DIR
        install/utils
    FILES
        config/module1.cfg
        Readme-module1.txt
)
```

Here `API_DIR` default for the package is set to `api`.

`SRC_DIR` is absent so the default value `.` is set for the module `module1`.

The package installation dir will be set to `install/utils` relative to the build root directory (`BUILD_ROOT_DIR`).

Two text files will be installed to the package installation dir.

## ▶`define_package_submodule()`
Declare a new **namespace** within the current package, thus effectively starting a **package sub-module**. The package must be previously declared with [`define_package()`](#define_package).

The idea behind the package sub-module is to introduce an additional level of namespace prefixes while using the same installation tree and, most importantly, the same export file as the entire package.

When `define_package_submodule()` is called, all subsequently declared targets receive the namespace prefix composed of the package name and the sub-module name.

### `define_package_submodule()` macro format
```cmake
define_package_submodule ( <module-name>
    FILES <install-misc-files-list>
)
```

### `define_package_submodule()` parameters

#### `FILES`
A list of miscellaneous (confguration, docs, plain text) files to be unconditionally installed to the package install root directory. Similar to [`define_package(FILES)`](#files).

### `define_package_submodule()` example
```cmake
define_package ( module1 . . . )
. . .
define_package_submodule ( utils
    FILES
        config/utils.cfg
)
```
After this call a new sub-module `utils` is decalred within the package `module1`. Within the package targets of this module may be referred either by their proper names, or by the alias names prefixed by `utils::` (the short namespace) or by `module1::utils::` (the full namespace).
```cmake
define_api ( numerics . . . )
define_api ( approx . . . )

define_lib ( curves
    LIBS
        module1::utils::numerics
        utils::approx
    . . .
```
Outside the package, after being imported, the targets from `module1` can be referred by the full namespaced prefixes only.
```cmake
define_package ( module2 . . . )
. . .
define_lib ( models . . .
    LIBS
        module1::utils::curves
    . . .
```
## ▶`get_package_install_paths`
Get the actual paths used during installation of the current 
package or submodule.

The paths for library, include, binary and aux subdirectories are returned in the user-supplied variables.
### `get_package_install_paths()` macro format
```cmake
    get_package_install_paths (
        API <api-path-var>
        LIB <lib-path-var>
        BIN <bin-path-var>
        AUX <aux-path-var>
    )
```

### `define_package_submodule()` example
```cmake
    get_package_install_paths (
        API api_install_path
        AUX aux_install_path
    )
    message("api: ${api_install_path}")
    message("aux: ${aux_install_path}")
```
may produce the following ouput:
```bash
api: install/include/module1
aux: install/module1
```

## ▶`use_package()`
Declare using a package previously declared with [`define_package()`](#define_package). 

This macro is supposed to work transparently both for the [single-tree and module-wise builds](#single-tree-and-module-wise-builds):

- if the external package was previously built separately, it is searched using the standard `CMake` mechanisms like `find_package()`;
- if both packages are built within the same meta-project (as in single-tree build), then it is actually not imported as a separate package, but all its targets are accessible with the package's namespace aliases.

**Note**: `use_package()` currently does not search for system or 3-rd party packages. It may sometimes discover such external packages but is guaranteed to work only with the packages created using `cmake-runner` within the same project. Those packages are supposed to be already built and installed, which requires modules buid orchestration, such as provided by [`build-runner`](https://gitlab.com/bladerunnerlabs/build-system/build-runner).

### `use_package()` macro format
```cmake
   use_package ( <package-name> )
```

## ▶`project_import_hints()`
Set a set of default locations in which `import_lib()` will search for libraries and include files.

### `project_import_hints()` macro format

```cmake
project_import_hints( <lib_name>
    PATH <general-path-hints-list>
    LIB_PATH <library-path-hints-list>
    INC_PATH <include-path-hints-list>
)
```
### `project_import_hints()` parameters

#### `PATH`
A general list of paths used for searching both libraries and include files.

#### `LIB_PATH`, `INC_PATH`
Lists of specific paths used for searching libraries and include files, accordingly.

## ▶`import_lib()`

### `import_lib()` macro format

```cmake
import_lib( <lib_name>
    CUSTOM
    EXTRA_NAMES <extra-names-list>
    PATH_HINTS <path-hints-list>
    PATH_SUFFIXES <path-suffixes-list>
    API_HEADERS <api-headers-list>
)
```

### `import_lib()` parameters

#### `CUSTOM`
Ignore the default system lib search paths set by CMake. Sets the option variables: `NO_CMAKE_PATH`, `NO_CMAKE_SYSTEM_PATH`

#### `EXTRA_NAMES`
A list of miscellaneous names under which the library under search may appear in the filesystem.

#### `PATH_HINTS`
A list of additional paths where the library may be potentially located. This list is added to the default paths list set previously by [`project_import_hints()`](#project_import_hints).

#### `PATH_SUFFIXES`
A list of suffixes to be added to the list of search paths. Some suffixes are tried automatically, e.g. the name of the library, `include`, `libs` etc. This option provides a list of extra hints.

**Example**: `linux x86-64 x86_64-linux-gnu`

#### `API_HEADERS`
A list of header file names associated with the library. By default `<lib-name>.h` is searched.

### `define_package()` and `use_package()` example
Let's assume we have `num-utils` library package, in its `CMakeLists.txt` there is the following section:
```cmake
    define_package ( num-utils )
    define_lib ( approximation
        ...
    )
```
Another package: `geometry`, consumes `num-utils`, in its `CMakeLists.txt` there is the following section:
```cmake
    define_package ( geometry )
    use_package ( num-utils )
    define_lib ( curve-fit
        . . .
        LIBS
            num-utils::approximation
    )
```

# Build target management macros

## ▶`define_build_class()`
Define a **build-class** with a list of its **variants**. 

A **build-class** is a group of build sub-types, or build **variants**, which share a common build environment but differ in a number of small details and paramerters. 

For example, all build-class variants may be built using the same compiler called with a common set of flags and linked to a common set of libraries etc. Each build-class variant may add a unique combination of flags, symbols and libraries to link with.

**Note**: defining build-classes is not required and is relevant only for special types of project with a certain structure of build and execution environments.

Using build-classes and their variants allows the user to provide a single definition for a build target (library, application or api), even if the target is meant to be built for multiple build variants.

Variant-specific targets are automatically generated and built, while inheriting the common configuration from the build-class and using their unique configuration details from the variant-specific definitions.

### `define_build_class()` macro format
```cmake
define_build_class ( <build-class-name>
    VARIANTS <variant-names-list>
)
```

### `define_build_class()` example
```cmake
define_build_class ( endian
    VARIANTS 
        big little 
)
define_build_class ( simulator
    VARIANTS 
        logical physical
)
```

## ▶`define_anchor()`
Used to define the current directory as an **anchor**. Further down the directory subtree new targets may expose their API (public) headers as relative to the anchor. Thus the anchor will play the role of an upper-level [`API_DIR`](#api_dir).

### `define_anchor()` macro format
```cmake
define_anchor ( <anchor-name> )
```
The only parameter `<anchor-name>` must be a valid and a unique target name.

## ▶`define_lib()`
Used to define a library target. Type of the library (static or shared) is determined by the standard CMake variables, like `BUILD_SHARED_LIBS`.

### `define_lib()` macro format
```cmake
define_lib ( <lib-target-name>
    BUILD_CLASS <build-class>
    GENERATE <variants-list | ALL>

    SRC_DIR <sources-dir>
    SOURCES <source-files-list>
    PRIV_HEADERS <private-headers-list>

    ANCHOR_API <anchor-name>
    API_DIR <public-headers-dir>
    API_HEADERS  <public-headers-list>

    DEFINITIONS <defined-symbols-list>
    COMPILER <compiler-options-list>
    LINKER <linker-options-list>

    LIBS <user-libraries-list>
    SYS_LIBS <system-libraries-list>

    INSTALL_DIR <lib-install-prefix>
    API_INSTALL_DIR <api-install-prefix>

    SHARED
)
```

### `define_lib()` parameters
The first positional argument `<lib-target-name>` is used directly to define the new library target name. It may be modified when `BUILD_CLASS` and `GENERATE` are passed (see below).  

#### `BUILD_CLASS`
Used to specify a build-class for which this target should be generated. Its variants are controlled by `GENERATE` keyword.

#### `GENERATE`
Used to specify the list of variants (must be related to the build-class specified by `BUILD_CLASS`) for which this target is going to be generated.

Variants may be specified:

- explicitly as a list of names; they must be defined previously as belonging to the requested build-class, or
- implicitly, when `ALL` is supplied; then all variants for the build-class are used.

For each variant a new target is generated: `<lib-target-name>-<build-class>-<variant-name>`. This means that when `BUILD_CLASS` is used, target `<lib-target-name>` is not created at all.

#### `SRC_DIR`
Used to specify the sub-dir hosting the source files and private headers for this target.

#### `SOURCES`
Used to specify the list of source files for this target.

#### `PRIV_HEADERS`
Used to specify the list of private files for this target. It is assumed that the private headers are located within the sub-tree of `SRC_DIR`.

#### `API_DIR`
Used to specify the sub-dir hosting the API (public) headers of the library.

All files specified under `API_HEADERS` will be searched and added from the API sub-dir. 

These files will be installed to the package-specific `include` installation dir without the `API_DIR` sub-dir prefix. This allows to refer to them consistently when including from a source file:

- when included from the source tree, `API_DIR` is automatically added as an include directory using `-I`;
- when included from the installation tree, the include file installation dir is automatically added as an include directory using `-I`.

This also means that if a header file is specified using a non-trivial path relative to `API_DIR`, that path will be the one the header should be referred with, further on.

**Example**:
```cmake
define_lib ( num_lib
    . . .
    API_DIR
        api
    API_HEADERS
        num_methods.h
        approx/interpolate.h
    . . .
```
Let's assume that `num_lib` is located in `${BUILD_ROOT_DIR}/my-project/utils/num_lib`, and the headers are installed to `${BUILD_ROOT_DIR}/install/my-project/include`.

Then if the headers are included from the source tree, the compiler is invoked with:
```shell
 -I ${BUILD_ROOT_DIR}/my-project/utils/num_lib/api
```
and if the headers are included from the installed package, then the compiler is invoked with:
```shell
 -I ${BUILD_ROOT_DIR}/install/my-project/include
```
In both cases the following source code should work as intended:
```c++
#include "num_methods.h"
#include "approx/interpolate.h"
```
#### `API_HEADERS`
Used to specify the list of API (public) headers residing in `API_DIR`.

#### `ANCHOR_API`
Recalculate the relative paths for API (public) headers from a directory associated with the anchor name, which must be created by [`define_anchor()`](#define_anchor).

**Example**:
Let's assume the following anchor is defined in `${BUILD_ROOT_}/my-project/utils/CMakeLists.txt`:
```cmake
define_anchor ( utils_anchor )
```
and then, in `${BUILD_ROOT_}/my-project/utils/math/num_lib/CMakeLists.txt`:
```cmake
define_lib ( num_lib
    . . .
    ANCHOR_API
        utils_anchor
    API_HEADERS
        num_methods.h
        approx/interpolate.h
    . . .
```
The headers are installed to `${BUILD_ROOT_DIR}/install/my-project/include/math/num_lib/approx/interpolate.h`. Thus the following source code should as intended when the headers are included either from the source tree or the install tree:
```c++
#include "math/num_lib/num_methods.h"
#include "math/num_lib/approx/interpolate.h"
```
This effect is obtained by vurtue of using the proper `-I` compiler directives, similar to the [example for `API_DIR`](#api_dir-1).

#### `DEFINITIONS`
Used to specify the list of preprocessor *symbols* to be defined for the target.

#### `COMPILER`
Used to specify the list of *compiler* options for the target.

#### `LINKER` 
Used to specify the list of *linker* options for the target.

**Example**:
```cmake
define_lib ( num_lib
    . . .
    DEFINITIONS
        EPSILON=0.01
    COMPILER
        -Wfloat-conversion -ffast-math
    LINKER
        -rdynamic
    . . .
```

#### `LIBS`
 Used to specify the list of libraries available within the project. These may be real libraries (static or shared) or APIs (interface libs). They may be either defined in the current project or imported from external projects, after being previously built and installed.

#### `SYS_LIBS`
 Used to specify the list of system libraries, assumed to be globally available - without resorting to CMake specific mechanisms like `find_package()` and `find_library()`.

#### `SHARED`
Forces the library to be built as a shared object.

#### `INSTALL_DIR`, `API_INSTALL_DIR`
`INSTALL_DIR`: Target-specific installation prefix to be added after the package installation prefix, for the library files.

`API_INSTALL_DIR`: similarly, the installation prefix for the public API header files.

**Example**: in the package `module1` there is a library: `lib1`, with the API header: `lib1_user.h`.
```cmake
define_package(module1)
. . .
define_lib( lib1
    . . .
    INSTALL_DIR
        lib64
    API_HEADERS
        lib1_user.h
    API_INSTALL_DIR
        misc/include
)
```

- If `INSTALL_DIR` is used as in the example above, the library file is installed to: `<install-dir>/module1/lib64/lib1.a`
  - by default the library file is installed to: `<install-dir>/module1/libs/lib1.a`;
  - Note that while by default shared libs are installed to `bin`, static libs to `libs`, when `INSTALL_DIR` is supplied, both types are installed to the requested path.
- If `API_INSTALL_DIR` is used, the API header file is installed to: `<install-dir>/module1/misc/include/lib1_user.h`
  - by default the API header file is installed to: `<install-dir>/module1/include/lib1_user.h`.

### `define_lib()` example
Examples above exemplified various isolated aspects of the macro. The following one shows a relatively standard use case:
```cmake
define_package(module1)
use_package(num-utils)
find_package(Boost 1.73
    REQUIRED
    COMPONENTS
        date_time filesystem system
)
. . .
define_lib( control
    SOURCES
        control_main.cpp
        control_utils.cpp
    PRIV_HEADERS
        control_priv.h
    API_DIR
        api
    API_HEADERS
        control.h
    DEFINITIONS
        USE_FFT=1
    LIBS
        module1::setup_api
        num-utils::fft
    SYS_LIB
        Boost::filesystem
        Boost::date_time
)
```
This library is built:

- from sources and headers residing in the current directory (the default `.` of `SRC_DIR` is used)
- exports a public header from `api` subdirectory
- defines a compilation symbol `USE_FFT`
- uses an API lib `setup_api` from the same package `module1`
- uses a library `fft` from an external package `num-utils`
- the rest of the libraries do not originate from the current project, thus they are considered `SYS_LIBS`:
    - two Boost libraries corresponding to Boost components: `filesystem`, `date-time`

The library target does not try to override the installation destination directories.

- library object is installed to: `<install-dir>/module1/lib/libcontrol.a`
- API header is installed to: `<install-dir>/module1/include/control.h`

## ▶`define_api()`

### `define_api()` macro format
```cmake
define_api ( <api-target-name>
    BUILD_CLASS <build-class>
    GENERATE <variants-list | ALL>

    API_DIR <public-headers-dir>
    ANCHOR_API <anchor-name>
    API_HEADERS  <public-headers-list>

    DEFINITIONS <defined-symbols-list>
    COMPILER <compiler-options-list>

    LIBS <user-libraries-list>

    API_INSTALL_DIR <api-install-prefix>
)
```

### `define_api()` parameters

#### `BUILD_CLASS`, `GENERATE`
See [`BUILD_CLASS`](#build_class), [`GENERATE`](#generate) keywords of [`define_lib()`](#define_lib).

#### `API_DIR`, `ANCHOR_API`, `API_HEADERS`
See [`API_DIR`](#api_dir-1), [`ANCHOR_API`](#anchor_api), (`API_HEADERS`)(#api_headers) keywords of [`define_lib()`](#define_lib).

Note that `SRC_DIR` and `SOURCES` keys are not defined for `define_api()` as the defined target is supposed to export public header files only.

#### `DEFINITIONS`, `COMPILER`, `LINKER`
See [`DEFINITIONS`](#definitions), [`COMPILER`](#compiler), [`LINKER`](#linker) keywords of [`define_lib()`](#define_lib).

#### `LIBS`
A list of libraries required for use by this target. These libraries may be regular libraries or APIs, also defined by `define_api()` macro.

When a target declares a dependency upona an external library, this dependectys is going to be transitively passed to the targets requesting the API target in their `LIBS` section.

#### `SYS_LIBS`
A list of system libraries, to be transitively passed to the targets requesting the API target in their `SYS_LIBS` section.

#### `SHARED`
Force building of a shared object.

#### `API_INSTALL_DIR`
`API_INSTALL_DIR`: Target-specific installation prefix to be added after the package installation prefix, for the API header files.

See also [`INSTALL_DIR`, `API_INSTALL_DIR`](#install_dir-api_install_dir) of [`define_lib()`](#define_lib) for comparison.

**Example**: in the package `module1` there is a API: `control_api`, with the API header: `control.h`.
```cmake
define_package(module1)
. . .
define_api( control_api
    . . .
    API_HEADERS
        control.h
    API_INSTALL_DIR
        misc/include
)
```

- If `API_INSTALL_DIR` is used as in the example above, the API header file is installed to: `<install-dir>/module1/misc/include/control.h`
  - by default the API header file is installed to: `<install-dir>/module1/include/control.h`.

### `define_api()` example
```cmake
define_package(module1)
use_package(num-utils)
. . .
define_api( setup_api
    API_DIR
        api
    API_HEADERS
        setup.h
        config/json-setup.h
    COMPILER
        -funsafe-math-optimizations
        -funroll-loops
    LIBS
        module1::control_api
        num-utils::approximation
    SYS_LIB
        zlib::zlib
        math
        rt
)
```
This API library consists of two headers from `api` subdirectory of the current `CMakeLists.txt` location. 

It defines a couple of compiler options and requires few libraries.

- Two libraries come from the current project and are considered `LIBS`:
    - `module1::control_api` from the same `module1` package;
    - `num-utils::approximation` from an external `num-utils` package.
- The rest of the libraries do not originate from the current project, thus they are considered `SYS_LIBS`:
    - `zlib::zlib` was importd either by [`find_package()`](https://cmake.org/cmake/help/latest/command/find_package.html) or [`import_lib()`](#import_lib)
    - `math`, `rt` are standard system libraries, which can be linked with just by using `-lmath -lrt`. 

## ▶`define_api_variant()`

### `define_api_variant()` macro format
```cmake
define_api_variant ( <api-target-name>
    BUILD_CLASS <build-class>
    VARIANT <variant-name>

    API_DIR <public-headers-dir>
    API_HEADERS  <public-headers-list>

    DEFINITIONS <defined-symbols-list>
    COMPILER <compiler-options-list>

    LIBS <user-libraries-list>
    SYS_LIBS <system-libraries-list>

    INSTALL_DIR <install-prefix>
)
```

### `define_api_variant()` parameters

#### `BUILD_CLASS` and `VARIANT`
Used to specify a build-class and variant for which this API target is extended. 

The build-class must be exactly the same one that was supplied as [`BUILD_CLASS`](#build_class) to the [`define_api()`](#define_api) call for the API target name being extended.

The variant must be one of the build-class variants passed to [`GENERATE`](#generate) (either explicitly by name or implicitly as `ALL`).

## ▶`define_app()`

### `define_app()` macro format
```cmake
define_app ( <app-target-name>
    BUILD_CLASS <build-class>
    GENERATE <variants-list | ALL>

    SRC_DIR <sources-dir>
    SOURCES <source-files-list>
    PRIV_HEADERS <private-headers-list> 

    DEFINITIONS <defined-symbols-list>
    COMPILER <compiler-options-list>
    LINKER <linker-options-list>

    LIBS <user-libraries-list>
    SYS_LIBS <system-libraries-list>

    INSTALL_DIR <install-prefix>
)
```
### `define_app()` parameters

#### `BUILD_CLASS` and `GENERATE`
See [`BUILD_CLASS`](#build_class), [`GENERATE`](#generate) keywords of [`define_lib()`](#define_lib).

#### `SRC_DIR`
Used to specify the sub-dir hosting the source files and private headers for this target. If not specified, a default value set in [`define_package()`](#src_dir).

#### `SOURCES`
Used to specify the list of source files for this target.

#### `PRIV_HEADERS`
Used to specify the list of private files for this target. It is assumed that the private headers are located within the sub-tree of `SRC_DIR`.

#### `DEFINITIONS`, `COMPILER`, `LINKER`
See [`DEFINITIONS`](#definitions), [`COMPILER`](#compiler), [`LINKER`](#linker) keywords of [`define_lib()`](#define_lib).

#### `LIBS`, `SYS_LIBS`
See [`LIBS`](#libs), [`SYS_LIBS`](#sys_libs) keywords of [`define_lib()`](#define_lib).

#### `INSTALL_DIR`
`INSTALL_DIR`: Target-specific installation prefix to be added after the package installation prefix, for the library files.

**Example**: in the package `module1` there is an application: `app1`.
```cmake
define_package(module1)
. . .
define_app( app1
    . . .
    INSTALL_DIR
        utils
)
```

- If `INSTALL_DIR` is used as in the example above, the application file is installed to: `<install-dir>/module1/utils/app1`
  - by default the application file is installed to: `<install-dir>/module1/bin/app1`;

#### Note on API headers
Applications do not export public headers to enable linking with them, as the libraries do. Thus the options like `API_DIR` and `API_HEADERS` are absent from `define_app()`.

At times it is necessary, though, to expose some common definitions for the development of other libraries and apps with which the application is going to interoperate. This is achieved using [`define_api()`](#define_api).

## ▶`define_utest()`

### `define_utest()` macro format
```cmake
define_utest( <utest-app-name>
    # the same parameters as in define_app()
)
```

## ▶`define_batch()` 

This macro is used to designate a group of files as a single installable unit. These may be a mix of text and configuration files and executables, like scripts.
 
### `define_batch()` macro format

```cmake
define_batch( <batch-app-name>
    FILES <regular-files-list>
    EXEC_FILES <executable-files-list>

    INSTALL_DIR <install-prefix>
    UTEST
)
```

### `define_batch()` parameters

#### `FILES`
Used to specify a list of regular (non-executable) files to be installed in the batch.

#### `EXEC_FILES`
Used to specify a list of executable files to be installed in the batch.

#### `INSTALL_DIR`
Target-specific installation prefix added after the package installation prefix. See [INSTALL_DIR of define_api()](#install_dir).

#### `UTEST`
Install this batch as a `utest` bundle.

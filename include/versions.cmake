# versions.cmake
#
# Copyright (c) 2019-2021 - BladeRunner Labs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software")
# to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# include dependencies
if (NOT _VERSIONS_CMAKE_RUNNER_INCLUDED)

if (NOT _MESSAGES_CMAKE_INCLUDED)
    include(${INC_CMAKE_DIR}/messages.cmake)
endif()

set( CMAKE_RUNNER_VERSION 1.5.0 )
msg_green("cmake-runner version: ${CMAKE_RUNNER_VERSION}")

function(cmake_runner_minimum_required)
    # parse arguments
    set(args_options )
    set(args_single VERSION)
    set(args_multi)
    cmake_parse_arguments("REQUIRED" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})

    if( REQUIRED_VERSION VERSION_GREATER CMAKE_RUNNER_VERSION )
        msg_error("cmake-runner version: ${CMAKE_RUNNER_VERSION}, while minimum: ${REQUIRED_VERSION} required")
    endif()
endfunction()

set_parent_var(_VERSIONS_CMAKE_RUNNER_INCLUDED YES)
endif()

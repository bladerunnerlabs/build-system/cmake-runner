# options.cmake
#
# Copyright (c) 2019-2021 - BladeRunner Labs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software")
# to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

if (NOT _MESSAGES_CMAKE_INCLUDED)
    include(${INC_CMAKE_DIR}/messages.cmake)
endif()

if (NOT _OPTIONS_CMAKE_INCLUDED)

# General unconditional Options
set(CMAKE_LEGACY_CYGWIN_WIN32 0) # for Windows IDEs

# String User Options
# set(BUILD_TYPE "Debug" CACHE STRING "Build type: Debug, Release")

if(V)
    set( CMAKE_VERBOSE_MAKEFILE ON)
    set(CMAKE_RULE_MESSAGES ON )
else()
    set(CMAKE_VERBOSE_MAKEFILE OFF)
    set(CMAKE_RULE_MESSAGES OFF)
endif()

unset(V CACHE)

msg_green("CMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}")
msg_green("BUILD_SHARED_LIBS=${BUILD_SHARED_LIBS}")
msg_green("CMAKE_VERBOSE_MAKEFILE=${CMAKE_VERBOSE_MAKEFILE}")
msg_green("CMAKE_RULE_MESSAGES=${CMAKE_RULE_MESSAGES}")

set_parent_var(_OPTIONS_CMAKE_INCLUDED YES)
endif(NOT _OPTIONS_CMAKE_INCLUDED)

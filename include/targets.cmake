# targets.cmake
#
# Copyright (c) 2019-2021 - BladeRunner Labs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software")
# to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# include dependencies
if (NOT _MESSAGES_CMAKE_INCLUDED)
    include(${INC_CMAKE_DIR}/messages.cmake)
endif()

if (NOT _TARGETS_CMAKE_INCLUDED)

# Multiple calls to export() command with same FILE
# without APPEND no longer allowed.
if(POLICY CMP0103)
  cmake_policy(SET CMP0103 OLD)
endif()

function(define_build_class class_name)
    # parse arguments
    set(args_options )
    set(args_single )
    set(args_multi VARIANTS)

    cmake_parse_arguments("BUILD_CLASS" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})
    if(BUILD_CLASS_UNPARSED_ARGUMENTS)
        msg_error("Unparsed arguments for define_build_class: ${BUILD_CLASS_UNPARSED_ARGUMENTS}")
    endif()

    list(APPEND BUILD_CLASSES_LIST ${class_name})
    set(BUILD_CLASSES_LIST ${BUILD_CLASSES_LIST} PARENT_SCOPE)

    set(${class_name}_VARIANTS_LIST ${BUILD_CLASS_VARIANTS} PARENT_SCOPE)

    msg_green("Added Build Class: ${class_name}, Variants: ${BUILD_CLASS_VARIANTS}")
endfunction() # define_build_class

macro(check_build_class)
    if (NOT ${${tgt_build_class}} IN_LIST BUILD_CLASSES_LIST)
        msg_error("Build class: ${${tgt_build_class}} (used by target: ${tgt_name}) undefined")
    endif()

    set(class_variants_list ${${tgt_build_class}}_VARIANTS_LIST)
    if (NOT ${class_variants_list})
        msg_error("${class_variants_list} (required by target: ${tgt_name}) undefined")
    endif()
endmacro()

macro(check_target_build_class_args
    tgt_name tgt_prefix
)
    set(tgt_build_class ${tgt_prefix}_BUILD_CLASS)
    set(tgt_generate ${tgt_prefix}_GENERATE)
    set(tgt_variant ${tgt_prefix}_VARIANT)

    if (${tgt_build_class})
        if (${tgt_generate} AND ${tgt_variant})
            msg_error("${tgt_name} defined both VARIANT and GENERATE")
        elseif(NOT ${tgt_generate} AND NOT ${tgt_variant})
            msg_error("${tgt_name} defined BUILD_CLASS but not VARIANT/GENERATE")
        endif()
    elseif(${tgt_generate})
        msg_error("${tgt_name} defined GENERATE but not BUILD_CLASS")
    elseif(${tgt_variant})
        msg_error("${tgt_name} defined VARIANT but not BUILD_CLASS")
    endif()
endmacro()

macro(set_target_build_class_variant
    tgt_name tgt_prefix
)
    set(tgt_build_class ${tgt_prefix}_BUILD_CLASS)
    set(tgt_variant ${tgt_prefix}_VARIANT)

    if (${tgt_build_class} AND ${tgt_variant})
        check_build_class()
        if (NOT ${tgt_variant} IN_LIST ${class_variants_list})
            msg_error("Variant: ${${tgt_variant}} of target: ${tgt_name} not in build class: ${${tgt_build_class}}")
        endif()
    endif()

    set(tgt_name "${tgt_name}_${${tgt_build_class}}_${${tgt_variant}}")
endmacro()

macro(set_target_variant_properties)
    if (${tgt_build_class})
        set_target_properties(${tgt_name} PROPERTIES
            BUILD_CLASS ${tgt_build_class}
            BUILD_VARIANT ${tgt_variant}
        )
    endif()
endmacro()

macro(argn_filter_sublist
        sublist_key sublist_var
        )
    set(ARGN_FILTERED ${ARGN})
    list(REMOVE_ITEM ARGN_FILTERED
            ${sublist_key} # remove keyword, e.g. GENERATE
            ${${sublist_var}}) # remove its parsed arguments
    list(JOIN ARGN_FILTERED " " ARGN_FILTERED_STR)
endmacro()

macro(check_unsupported_arg
    arg_name # name (key) of the argument to check
    prefix # parsing variable prefix
    tgt_arg_name # target var name, 1st arg to the original function
)
    set(var_arg_name "${prefix}_${arg_name}")
    if(${var_arg_name})
        set(tgt_name ${${tgt_arg_name}})
        msg_error("Unsupported argument: ${arg_name} in: ${CMAKE_CURRENT_FUNCTION}(${tgt_name}...) ")
    endif()
endmacro()

macro(check_args
    prefix # parsing variable prefix
    tgt_arg_name # target var name, 1st arg to the original function
)
    LIST(APPEND args_options ${args_unsupported})
    set(unparsed_args ${${prefix}_UNPARSED_ARGUMENTS})
    if(${unparsed_args})
        msg_error("Unexpected arguments for ${CMAKE_CURRENT_FUNCTION}(${tgt_arg}: ${unparsed_args}")
    endif()
    foreach( arg ${args_unsupported})
        check_unsupported_arg(${arg} ${prefix} ${tgt_arg_name})
    endforeach(arg)
endmacro()

macro(check_target_generated_variants
    tgt_name tgt_prefix
    # arguments list from original target definition is passed here unnamed
)
    set(tgt_build_class ${tgt_prefix}_BUILD_CLASS)
    set(tgt_generate ${tgt_prefix}_GENERATE)

    if (${tgt_build_class} AND ${tgt_generate})
        check_build_class()

        list(JOIN ${tgt_generate} " " ${tgt_generate}_STR)
        if (${${tgt_generate}_STR} STREQUAL "ALL")
            set(${tgt_generate} ${${class_variants_list}}) # use all globally defined variants
        else()
            foreach(v ${${tgt_generate}})
                if (NOT v IN_LIST ${class_variants_list})
                    msg_error("Generate variant: ${v} (used by target: ${tgt_name}) not in build class: ${tgt_build_class}")
                endif()
            endforeach()
        endif()

        argn_filter_sublist("GENERATE" ${tgt_generate}_STR ${ARGN})
    endif()
endmacro()

macro(create_namespace_aliases tgt_name)
    if (SUBMODULE_NAMESPACE_NAME)
        # create aliases with the package module namespace
        add_library(${NAMESPACE_NAME}${SUBMODULE_NAMESPACE_NAME}${tgt_name}
            ALIAS ${tgt_name}
        )
        add_library(${SUBMODULE_NAMESPACE_NAME}${tgt_name}
            ALIAS ${tgt_name}
        )
    else()
        # create alias with the package namespace
        add_library(${NAMESPACE_NAME}${tgt_name}
            ALIAS ${tgt_name}
        )
    endif()
endmacro()

macro(create_relative_sources prefix)
    if(NOT ${SRC_DIR} STREQUAL ".")
        foreach(src ${${prefix}_SOURCES})
            list(APPEND RELATIVE_SOURCES "${SRC_DIR}/${src}")
        endforeach(src)
    else()
        set(RELATIVE_SOURCES ${${prefix}_SOURCES})
    endif()
endmacro()

macro(create_relative_priv_headers prefix)
    if(NOT ${SRC_DIR} STREQUAL ".")
        foreach(priv_hdr ${${prefix}_PRIV_HEADERS})
            list(APPEND RELATIVE_PRIV_HEADERS "${SRC_DIR}/${priv_hdr}")
        endforeach(priv_hdr)
    else()
        set(RELATIVE_PRIV_HEADERS ${${prefix}_PRIV_HEADERS})
    endif()
endmacro()

macro(define_anchor anchor_name)
    add_custom_target(${anchor_name})
    set(ANCHOR_${anchor_name} ${CMAKE_CURRENT_SOURCE_DIR})
    msg_blue("ANCHOR ${anchor_name}: ${ANCHOR_${anchor_name}}")
endmacro() # define_anchor

macro(handle_anchor)
    if(anchor_name)
        if(NOT TARGET ${anchor_name})
            msg_error("Attempt to use anchor: ${anchor_name} which does not exist")
        endif()
        set(anchor_path ANCHOR_${anchor_name})
        if(NOT ${anchor_path})
            msg_error("No anchor path saved for target: ${anchor_name}")
        endif()
        set(base_api_dir ${${anchor_path}})

        # with anchors we need to keep entire path
        # from the anchor to a header file installed
        # thus API_DIR must be re-created in the installation tree
        set(API_DIR_KEEP ON)

        file(RELATIVE_PATH api_dir_from_base
            ${base_api_dir}
            ${CMAKE_CURRENT_SOURCE_DIR})

        message("${tgt_name}: use anchor ${anchor_name}: ${base_api_dir} + ${api_dir_from_base}")
    else()
        set(base_api_dir ${CMAKE_CURRENT_SOURCE_DIR})
        set(api_dir_from_base )
    endif()

    if(NOT ${API_DIR} STREQUAL ".")
        foreach(hdr ${API_HEADERS})
            list(APPEND RELATIVE_API_HEADERS "${API_DIR}/${hdr}")
        endforeach(hdr)
        set(base_api_dir ${base_api_dir}/${API_DIR})
    else()
        set(RELATIVE_API_HEADERS ${API_HEADERS})
    endif()

    set(BUILD_IFACE_INC_DIR ${base_api_dir})
    message("${tgt_name}: API headers build-interface include path: ${BUILD_IFACE_INC_DIR}")

    # installed target's include root, from PACKAGE_INSTALL_INC_PREFIX
    set(INSTALL_IFACE_INC_DIR ${PACKAGE_INSTALL_INC_PREFIX})
    message("${tgt_name}: API headers install-interface include path: ${INSTALL_IFACE_INC_DIR}")

    # install prefix for API headers
    set(TGT_HDRS_INSTALL_PREFIX ${PACKAGE_INSTALL_INC_PREFIX})
    if(api_dir_from_base)
        set(TGT_HDRS_INSTALL_PREFIX ${TGT_HDRS_INSTALL_PREFIX}/${api_dir_from_base})
    endif()
    if(API_DIR_KEEP AND NOT ${API_DIR} STREQUAL ".")
        set(TGT_HDRS_INSTALL_PREFIX ${TGT_HDRS_INSTALL_PREFIX}/${API_DIR})
    endif()
    message("${tgt_name}: API headers install prefix: ${TGT_HDRS_INSTALL_PREFIX}")
endmacro() # handle_anchor

macro(install_api_headers)

    foreach(hdr ${API_HEADERS})
        if(NOT ${API_DIR} STREQUAL ".")
            set(hdr_src_path ${API_DIR}/${hdr})
        else()
            set(hdr_src_path ${hdr})
        endif()

        set(hdr_install_path
            ${TGT_HDRS_INSTALL_PREFIX}/${hdr})

        get_filename_component(hdr_install_dir
            ${hdr_install_path} DIRECTORY)

        install(FILES ${hdr_src_path}
            DESTINATION ${hdr_install_dir})

        message("install ${hdr} to ${hdr_install_dir}")
    endforeach(hdr)
endmacro()

function(define_lib lib_name)
    # parse arguments
    set(args_options SHARED API_DIR_KEEP)
    set(args_single BUILD_CLASS VARIANT API_DIR ANCHOR_API SRC_DIR)
    set(args_multi SOURCES PRIV_HEADERS API_HEADERS 
            DEFINITIONS COMPILER LINKER LIBS SYS_LIBS GENERATE)

    # usupported arguments
    set(args_unsupported UTEST)

    LIST(APPEND args_options ${args_unsupported})
    cmake_parse_arguments("LIB" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})
    check_args("LIB" "lib_name")

    set(tgt_name ${lib_name})
    msg_yellow("define_lib ${tgt_name}")

    # handle build-class and variants, if any
    check_target_build_class_args(${tgt_name} "LIB")
    if (LIB_BUILD_CLASS)
        if (LIB_GENERATE)
            check_target_generated_variants(${tgt_name} "LIB" ${ARGN})
            # generate all requested targets
            project(${lib_name})
            msg_blue("LIBRARY: ${lib_name}, build class: ${LIB_BUILD_CLASS}, generate: ${LIB_GENERATE}" )
            foreach(v ${LIB_GENERATE})
                msg_blue("generate target: ${tgt_name}, build class: ${LIB_BUILD_CLASS}, variant: ${v}")
                define_lib(${tgt_name} ${ARGN_FILTERED} VARIANT ${v})
            endforeach()
            # generated targets defined recursively, nothing to do
            return()
        elseif (LIB_VARIANT)
            set_target_build_class_variant(${tgt_name} "LIB")
            # continue with the target definition
        endif()
    endif()

    # transform some arguments
    if(LIB_API_DIR)
        set(API_DIR ${LIB_API_DIR})
    endif()
    if(LIB_SRC_DIR)
        set(SRC_DIR ${LIB_SRC_DIR})
    endif()

    create_relative_sources(LIB)
    create_relative_priv_headers(LIB)

    set(API_HEADERS ${LIB_API_HEADERS})
    set(API_DIR_KEEP ${LIB_API_DIR_KEEP})

    set(anchor_name ${LIB_ANCHOR_API})
    handle_anchor()

    if (LIB_SHARED)
        set(USE_SHARED SHARED)
    endif()

    add_library(${tgt_name}
        ${USE_SHARED}
        ${RELATIVE_SOURCES}
        ${RELATIVE_PRIV_HEADERS}
        ${RELATIVE_API_HEADERS}
    )
    # set_target_variant_properties()

    set_source_files_properties(
        ${RELATIVE_API_HEADERS} ${RELATIVE_PRIV_HEADERS} PROPERTIES
        HEADER_FILE_ONLY ON
    )

    create_namespace_aliases(${tgt_name})

    # add compile-time definitions
    target_compile_definitions(${tgt_name} PUBLIC
        ${LIB_DEFINITIONS}
    )
    # add compiler options
    target_compile_options(${tgt_name} PUBLIC
        ${LIB_COMPILER}
    )
    # define include dirs for private and public use
    target_include_directories(${tgt_name} PRIVATE
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    )
    target_include_directories(${tgt_name} PUBLIC
        $<BUILD_INTERFACE:${BUILD_IFACE_INC_DIR}>
    )

    target_include_directories(${tgt_name} INTERFACE
        $<INSTALL_INTERFACE:${INSTALL_IFACE_INC_DIR}>
    )

    if (${tgt_build_class})
        foreach(t ${LIB_LIBS})
            set(variant_target_name ${t}_${${tgt_build_class}}_${${tgt_variant}})
            if (TARGET ${variant_target_name})
                list(APPEND VARIANT_LIBS ${variant_target_name})
                message("${tgt_name}: lib ${t} --> ${variant_target_name}")
            else()
                list(APPEND VARIANT_LIBS ${t})
            endif()
        endforeach()
        set(LIB_LIBS ${VARIANT_LIBS})
    endif()

    #define libraries to link with
    target_link_libraries(${tgt_name}
        ${LIB_LIBS}
        ${LIB_SYS_LIBS}
        "${LIB_LINKER}"
    )

    if (NOT PACKAGE_NOINSTALL)
        # define install policies
        install(TARGETS ${tgt_name}
            EXPORT ${EXPORT_NAME}
            ARCHIVE DESTINATION ${PACKAGE_INSTALL_LIB_PREFIX}
            LIBRARY DESTINATION ${PACKAGE_INSTALL_LIB_PREFIX}
            RUNTIME DESTINATION ${PACKAGE_INSTALL_BIN_PREFIX}
        )

        #set_target_properties(${tgt_name} PROPERTIES
        #    INSTALL_RPATH ${PACKAGE_INSTALL_LIB_PREFIX}
        #)

        # install header files
        install_api_headers()
    endif()

    msg_green("Added LIBRARY: ${NAMESPACE_NAME}${SUBMODULE_NAMESPACE_NAME}${tgt_name}")
endfunction() # define_lib

function(define_api api_name)
    # parse arguments
    set(args_options API_DIR_KEEP)
    set(args_single BUILD_CLASS VARIANT API_DIR ANCHOR_API API_INSTALL_DIR)
    set(args_multi API_HEADERS DEFINITIONS COMPILER LIBS SYS_LIBS GENERATE)

    # usupported arguments
    set(args_unsupported SRC_DIR SOURCES PRIV_HEADERS LINKER)

    LIST(APPEND args_options ${args_unsupported})
    cmake_parse_arguments("API" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})
    check_args("API" "api_name")

    set(tgt_name ${api_name})
    msg_yellow("define_api ${tgt_name}")

    # handle build-class and variants, if any
    check_target_build_class_args(${tgt_name} "API")
    if (API_BUILD_CLASS)
        if (API_GENERATE)
            check_target_generated_variants(${tgt_name} "API" ${ARGN})
            # generate all requested targets
            project(${api_name})
            msg_blue("API: ${api_name}, build class: ${API_BUILD_CLASS}, generate: ${API_GENERATE}" )
            foreach(v ${API_GENERATE})
                msg_blue("generate target: ${tgt_name}, build class: ${API_BUILD_CLASS}, variant: ${v}")
                define_api(${tgt_name} ${ARGN_FILTERED} VARIANT ${v})
            endforeach()
            # generated targets defined recursively, nothing to do
            return()
        elseif (API_VARIANT)
            set_target_build_class_variant(${tgt_name} "API")
            # continue with the target definition
        endif()
    endif()

    # transform some arguments
    if(API_API_DIR)
        set(API_DIR ${API_API_DIR})
    endif()

    if (NOT API_API_INSTALL_DIR)
        set(API_API_INSTALL_DIR include)
    endif()

    set(API_HEADERS ${API_API_HEADERS})
    set(API_DIR_KEEP ${API_API_DIR_KEEP})

    set(anchor_name ${API_ANCHOR_API})
    handle_anchor()

    add_library(${tgt_name} INTERFACE)
    #set_target_variant_properties()

    create_namespace_aliases(${tgt_name})

    # define include dirs for private and public use
    target_include_directories(${tgt_name} INTERFACE
        $<BUILD_INTERFACE:${BUILD_IFACE_INC_DIR}>
        $<INSTALL_INTERFACE:${INSTALL_IFACE_INC_DIR}>
    )
    # add compile-time definitions
    target_compile_definitions(${tgt_name} INTERFACE
        ${API_DEFINITIONS}
    )
    # add compiler options
    target_compile_options(${tgt_name} INTERFACE
        ${API_COMPILER}
    )
    # define libraries to link with
    target_link_libraries(${tgt_name} INTERFACE
        ${API_LIBS}
        ${API_SYS_LIBS}
    )

    if (NOT PACKAGE_NOINSTALL)
        # define install policies
        install(TARGETS ${tgt_name}
            EXPORT ${EXPORT_NAME}
            INCLUDES ${INSTALL_IFACE_INC_DIR}
        )

        # install header files
        install_api_headers()
    endif()

    msg_green("Added API: ${NAMESPACE_NAME}${SUBMODULE_NAMESPACE_NAME}${tgt_name}")
endfunction() # define_api

function(define_api_variant api_name)
    # parse arguments
    set(args_options API_DIR_KEEP)
    set(args_single BUILD_CLASS VARIANT API_DIR)
    set(args_multi DEFINITIONS COMPILER API_HEADERS LIBS SYS_LIBS)

    # usupported arguments
    set(args_unsupported SRC_DIR SOURCES PRIV_HEADERS LINKER INSTALL_DIR API_INSTALL_DIR)

    LIST(APPEND args_options ${args_unsupported})
    cmake_parse_arguments("API" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})
    check_args("API" "api_name")

    set_target_build_class_variant(${api_name} "API") # also sets tgt_name
    msg_yellow("define_api_variant ${tgt_name}")

    # transform some arguments
    if(API_API_DIR)
        set(API_DIR ${API_API_DIR})
    endif()

    set(API_HEADERS ${API_API_HEADERS})
    set(API_DIR_KEEP ${API_API_DIR_KEEP})

    set(anchor_name ${API_ANCHOR_API})
    handle_anchor()

    # define include dirs for private and public use
    target_include_directories(${tgt_name} INTERFACE
        $<BUILD_INTERFACE:${BUILD_IFACE_INC_DIR}>
        $<INSTALL_INTERFACE:${INSTALL_IFACE_INC_DIR}>
    )
    # add compile-time definitions
    target_compile_definitions(${tgt_name} INTERFACE
        ${API_DEFINITIONS}
    )
    # add compiler options
    target_compile_options(${tgt_name} INTERFACE
        ${API_COMPILER}
    )
    # define libraries to link with
    target_link_libraries(${tgt_name} INTERFACE
        ${API_LIBS}
        ${API_SYS_LIBS}
    )

    if (NOT PACKAGE_NOINSTALL)
        # install header files
        install_api_headers()
    endif()

    msg_green("API variant: ${NAMESPACE_NAME}${SUBMODULE_NAMESPACE_NAME}${tgt_name}")
endfunction() # define_api_variant

function(define_app app_name)
    # parse arguments
    set(args_options UTEST)
    set(args_single BUILD_CLASS VARIANT SRC_DIR INSTALL_DIR)
    set(args_multi SOURCES PRIV_HEADERS DEFINITIONS COMPILER LINKER
            LIBS SYS_LIBS FILES GENERATE)
    # usupported arguments
    set(args_unsupported API_DIR API_HEADERS ANCHOR_API
            API_INSTALL_DIR SHARED)

    LIST(APPEND args_options ${args_unsupported})
    cmake_parse_arguments("APP" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})
    check_args("APP" "app_name")

    set(tgt_name ${app_name})
    msg_yellow("define_app ${tgt_name}")

    # handle build-class and variants, if any
    check_target_build_class_args(${tgt_name} "APP")
    if (APP_BUILD_CLASS)
        if (APP_GENERATE)
            check_target_generated_variants(${tgt_name} "APP" ${ARGN})
            # generate all requested targets
            # declare new app project
            project(${app_name})
            msg_blue("APP: ${app_name}, build class: ${APP_BUILD_CLASS}, generate: ${APP_GENERATE}" )
            foreach(v ${APP_GENERATE})
                msg_blue("generate target: ${tgt_name}, build-class: ${APP_BUILD_CLASS}, variant: ${v}")
                define_app(${tgt_name} ${ARGN_FILTERED} VARIANT ${v})
            endforeach()
            # generated targets defined recursively, nothing to do
            return()
        elseif (APP_VARIANT)
            set_target_build_class_variant(${tgt_name} "APP")
            # continue with the target definition
        endif()
    endif()

    if(APP_SRC_DIR)
        set(SRC_DIR ${APP_SRC_DIR})
    endif()

    create_relative_sources(APP)
    create_relative_priv_headers(APP)

    add_executable(${tgt_name}
        ${RELATIVE_SOURCES}
        ${RELATIVE_PRIV_HEADERS}
    )
    #set_target_variant_properties()

    # define include dirs for private and public use
    target_include_directories(${tgt_name} PRIVATE
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    )

    target_compile_definitions(${tgt_name} PRIVATE
        ${APP_DEFINITIONS}
    )
    # add compiler options
    target_compile_options(${tgt_name} PRIVATE
        ${APP_COMPILER}
    )

    if (${tgt_build_class})
        foreach(t ${APP_LIBS})
            set(variant_target_name ${t}_${${tgt_build_class}}_${${tgt_variant}})
            if (TARGET ${variant_target_name})
                list(APPEND VARIANT_LIBS ${variant_target_name})
                message("${tgt_name}: lib ${t} --> ${variant_target_name}")
            else()
                list(APPEND VARIANT_LIBS ${t})
            endif()
        endforeach()
        set(APP_LIBS ${VARIANT_LIBS})
    endif()

    #define libraries to link with
    target_link_libraries(${tgt_name}
        ${APP_LIBS}
        ${APP_SYS_LIBS}
        "${APP_LINKER}" # linker options added as libs
    )

    #set_target_properties(${tgt_name} PROPERTIES
    #    INSTALL_RPATH ${PACKAGE_INSTALL_LIB_PREFIX}
    #)

    set(APP_INSTALL_BIN_PREFIX ${PACKAGE_INSTALL_BIN_PREFIX})
    if (APP_INSTALL_DIR)
        set(APP_INSTALL_BIN_PREFIX ${APP_INSTALL_BIN_PREFIX}/${APP_INSTALL_DIR})
    endif()

    if (NOT PACKAGE_NOINSTALL)
        install(TARGETS ${tgt_name}
            EXPORT ${EXPORT_NAME}
            RUNTIME DESTINATION ${APP_INSTALL_BIN_PREFIX}
        )
        install(FILES ${APP_FILES}
            DESTINATION ${APP_INSTALL_BIN_PREFIX}
        )
    endif()

    if(NOT APP_UTEST)
        msg_green("Added APPLICATION: ${tgt_name}")
    else()
        msg_green("Added UNIT TEST: ${tgt_name}")
    endif()
endfunction() # define_app

function(define_lib_extra_sources lib_name)
    # parse arguments
    set(args_options )
    set(args_single SRC_DIR API_DIR ANCHOR_API API_INSTALL_DIR)
    set(args_multi SOURCES PRIV_HEADERS API_HEADERS)

    # usupported arguments
    set(args_unsupported SHARED UTEST BUILD_CLASS VARIANT GENERATE
        INSTALL_DIR LIBS SYS_LIBS FILES)

    LIST(APPEND args_options ${args_unsupported})
    cmake_parse_arguments("LIB_EXTRA" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})
    check_args("LIB_EXTRA" "lib_name")

    set(tgt_name ${lib_name})
    msg_yellow("define_lib_extra_sources ${tgt_name}")
    if (NOT TARGET ${tgt_name})
        msg_error("${CMAKE_CURRENT_FUNCTION}, target not found: ${tgt_name}")
    endif()

    # transform some arguments
    if(LIB_EXTRA_API_DIR)
        set(API_DIR ${LIB_EXTRA_API_DIR})
    endif()
    if(LIB_EXTRA_SRC_DIR)
        set(SRC_DIR ${LIB_EXTRA_SRC_DIR})
    endif()

    create_relative_sources(LIB_EXTRA)
    create_relative_priv_headers(LIB_EXTRA)

    target_sources(${tgt_name} PRIVATE
        ${RELATIVE_SOURCES}
        ${RELATIVE_PRIV_HEADERS}
    )

    set_source_files_properties(
        ${RELATIVE_API_HEADERS} ${RELATIVE_PRIV_HEADERS} PROPERTIES
        HEADER_FILE_ONLY ON
    )

    set(API_HEADERS ${LIB_EXTRA_API_HEADERS})
    set(API_DIR_KEEP ${LIB_EXTRA_API_DIR_KEEP})

    set(anchor_name ${LIB_EXTRA_ANCHOR_API})
    handle_anchor()

    if (NOT PACKAGE_NOINSTALL)
        # install header files
        install_api_headers()
    endif()

    get_target_property(tgt_source_dir ${tgt_name} SOURCE_DIR)
    file(RELATIVE_PATH rel_source_dir
            ${tgt_source_dir}
            ${CMAKE_CURRENT_SOURCE_DIR})

    msg_green("LIBRARY: ${tgt_name} added sources from: ${rel_source_dir}")
endfunction() # define_lib_extra_sources

function(define_app_extra_sources app_name)
    # parse arguments
    set(args_options )
    set(args_single SRC_DIR)
    set(args_multi SOURCES PRIV_HEADERS)
    set(args_unsupported
        API_DIR API_HEADERS ANCHOR_API API_INSTALL_DIR SHARED UTEST
        BUILD_CLASS VARIANT INSTALL_DIR LIBS SYS_LIBS GENERATE FILES)

    LIST(APPEND args_options ${args_unsupported})
    cmake_parse_arguments("APP_EXTRA" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})
    check_args("APP_EXTRA" "app_name")

    set(tgt_name ${app_name})
    msg_yellow("define_app_extra_sources ${tgt_name}")
    if (NOT TARGET ${tgt_name})
        msg_error("${CMAKE_CURRENT_FUNCTION}, target not found: ${tgt_name}")
    endif()

    create_relative_sources(APP_EXTRA)
    create_relative_priv_headers(APP_EXTRA)

    target_sources(${tgt_name} PRIVATE
        ${RELATIVE_SOURCES}
        ${RELATIVE_PRIV_HEADERS}
    )

    get_target_property(tgt_source_dir ${tgt_name} SOURCE_DIR)
    file(RELATIVE_PATH rel_source_dir
            ${tgt_source_dir}
            ${CMAKE_CURRENT_SOURCE_DIR})

    msg_green("APP: ${tgt_name} added sources from: ${rel_source_dir}")
endfunction() # define_app_extra_sources

function(define_utest utest_name)
    define_app(
        ${ARGV}
        UTEST
        INSTALL_DIR
            utest
    )
endfunction() # define_utest

function(define_batch batch_name)
    # parse arguments
    set(args_options UTEST)
    set(args_single INSTALL_DIR)
    set(args_multi FILES PROGRAMS DIRECTORY)
    set(args_unsupported API_DIR SRC_DIR SOURCES
            API_HEADERS PRIV_HEADERS
            DEFINITIONS COMPILER LINKER)

    if (PACKAGE_NOINSTALL)
        msg_yellow("BATCH ${batch_name} SKIPPED - NOINSTALL set")
        return()
    endif()

    msg_yellow("define_batch ${batch_name}")

    LIST(APPEND args_options ${args_unsupported})
    cmake_parse_arguments("BATCH" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})
    check_args("BATCH" "batch_name")

    if(NOT BATCH_INSTALL_DIR)
        set(BATCH_INSTALL_DIR ${batch_name})
    endif()

    set(BATCH_INSTALL_PREFIX ${PACKAGE_INSTALL_PREFIX_PLAIN})
    if (NOT ${BATCH_INSTALL_DIR} STREQUAL ".")
        set(BATCH_INSTALL_PREFIX ${BATCH_INSTALL_PREFIX}/${BATCH_INSTALL_DIR})
    endif()

    install(FILES ${BATCH_FILES}
        DESTINATION ${BATCH_INSTALL_PREFIX}
    )

    install(PROGRAMS ${BATCH_PROGRAMS}
        DESTINATION ${BATCH_INSTALL_PREFIX}
    )

    install(DIRECTORY ${BATCH_DIRECTORY}
        DESTINATION ${BATCH_INSTALL_PREFIX}
        USE_SOURCE_PERMISSIONS
    )

    message("${batch_name} install prefix: ${BATCH_INSTALL_PREFIX}")

    if(NOT BATCH_UTEST)
        msg_green("Added BATCH: ${batch_name}")
    else()
        msg_green("Added UNIT TEST SCRIPT FILES: ${batch_name}")
    endif()
endfunction() # define_batch

function(define_utest_script script_name)
    define_batch(
        ${ARGV}
        UTEST
        INSTALL_DIR
            utest
    )
endfunction() # define_utest_script

set_parent_var(_TARGETS_CMAKE_INCLUDED YES)
endif()

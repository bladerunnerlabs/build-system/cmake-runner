# packages.cmake
#
# Copyright (c) 2019-2021 - BladeRunner Labs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software")
# to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

if (NOT _MESSAGES_CMAKE_INCLUDED)
    include(${INC_CMAKE_DIR}/messages.cmake)
endif()

if (NOT _PACKAGES_CMAKE_INCLUDED)

macro(_install_export)
    install(EXPORT ${EXPORT_NAME}
        NAMESPACE ${NAMESPACE_NAME}${SUBMODULE_NAMESPACE_NAME}
        DESTINATION ${PACKAGE_NAME}
        FILE ${EXPORT_FILE}
    )
    export(EXPORT ${EXPORT_NAME}
        NAMESPACE ${NAMESPACE_NAME}${SUBMODULE_NAMESPACE_NAME}
        FILE ${EXPORT_FILE}
    )
endmacro()

macro(get_package_install_prefix
    out_var install_type_dir)

    set(${out_var} ${install_type_dir})
    if (_PACKAGE_INSTALL_PREFIX_PRE)
        if (${out_var})
            set(${out_var} ${_PACKAGE_INSTALL_PREFIX_PRE}/${${out_var}})
        else()
            set(${out_var} ${_PACKAGE_INSTALL_PREFIX_PRE})
        endif()
    endif()
    if (_PACKAGE_INSTALL_PREFIX_POST)
        if (${out_var})
            set(${out_var} ${${out_var}}/${_PACKAGE_INSTALL_PREFIX_POST})
        else()
            set(${out_var} ${_PACKAGE_INSTALL_PREFIX_POST})
        endif()
    endif()
endmacro()

macro(get_package_install_paths)
    # parse arguments
    set(args_options )
    set(args_single API LIB BIN AUX)
    set(args_multi )

    cmake_parse_arguments("VAR" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})

    if (VAR_API)
        get_package_install_prefix(${VAR_API} ${PACKAGE_API_INSTALL_DIR})
    endif()
    if (VAR_LIB)
        get_package_install_prefix(${VAR_LIB} ${PACKAGE_LIB_INSTALL_DIR})
    endif()
    if (VAR_BIN)
        get_package_install_prefix(${VAR_BIN} ${PACKAGE_BIN_INSTALL_DIR})
    endif()
    if (VAR_AUX)
        set(${VAR_AUX} ${PACKAGE_INSTALL_PREFIX_PLAIN})
    endif()
endmacro()

macro(get_install_prefix
    var_name subdir
)
    msg_red("DEPRECATED: get_install_prefix - use get_package_install_paths()")
    if (${subdir} STREQUAL "lib")
        get_package_install_paths(LIB ${var_name})
    elseif(${subdir} STREQUAL "include")
        get_package_install_paths(API ${var_name})
    elseif(${subdir} STREQUAL "bin")
        get_package_install_paths(BIN ${var_name})
    else()
        msg_error("Unsupported install type subdir: ${subdir}")
    endif()
endmacro()

macro(define_package name)
    # parse arguments
    set(args_options SUBMODULES NOEXPORT NOINSTALL)
    set(args_single API_DIR SRC_DIR INSTALL_DIR INSTALL_SCHEME
        API_INSTALL_DIR LIB_INSTALL_DIR BIN_INSTALL_DIR)
    set(args_multi FILES)

    cmake_parse_arguments("PACKAGE" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})

    if(PACKAGE_UNPARSED)
        message("Unparsed arguments for define_package(${name}: ${PACKAGE_UNPARSED}")
    endif()

    msg_green("\ndefine_package ${name}")

    if (NOT ${PROJECT_NAME} STREQUAL ${name})
        get_property(_PROJ_LANGS GLOBAL PROPERTY ENABLED_LANGUAGES)
        message("languages: ${_PROJ_LANGS}")
        project(${name} LANGUAGES ${_PROJ_LANGS})
    endif()

    if(NOT BUILD_ROOT_DIR)
        msg_error("BUILD_ROOT_DIR not set")
    endif()

    if( NOT CMAKE_INSTALL_PREFIX )
        set( CMAKE_INSTALL_PREFIX ${BUILD_ROOT_DIR}/install) # Default
    endif()

    if( NOT CMAKE_PREFIX_PATH )
        set(CMAKE_PREFIX_PATH ${CMAKE_INSTALL_PREFIX})
    endif()

    if( NOT CMAKE_MODULE_PATH )
        set(CMAKE_MODULE_PATH ${CMAKE_PREFIX_PATH})
    endif()

    unset(PACKAGE_NAME CACHE)
    unset(PACKAGE_WITH_SUBMODULES CACHE)
    unset(SUBMODULE_NAME CACHE)
    unset(SUBMODULE_NAMESPACE_NAME CACHE)

    set(PACKAGE_NAME ${name})
    msg_blue("PACKAGE: ${PACKAGE_NAME}")

    # RPATH for the build tree
    set(CMAKE_SKIP_BUILD_RPATH FALSE) # generate (don't skip) RPATH in build
    set(CMAKE_BUILD_RPATH_USE_ORIGIN FALSE) # use $ORIGIN in build RPATH
    set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE) # don't use install RPATH

    if (NOT PACKAGE_NOINSTALL)
        if ( NOT PACKAGE_INSTALL_DIR)
            set(PACKAGE_INSTALL_DIR ${PACKAGE_NAME})
        endif()

        if ( NOT PACKAGE_INSTALL_SCHEME)
            set(PACKAGE_INSTALL_SCHEME "package-last")
        endif()

        if ( NOT PACKAGE_API_INSTALL_DIR)
            set(PACKAGE_API_INSTALL_DIR "include")
        endif()

        if ( NOT PACKAGE_LIB_INSTALL_DIR)
            set(PACKAGE_LIB_INSTALL_DIR "lib")
        endif()

        if ( NOT PACKAGE_BIN_INSTALL_DIR)
            set(PACKAGE_BIN_INSTALL_DIR "bin")
        endif()

        # package root installation dir
        if ( ${PACKAGE_INSTALL_SCHEME} STREQUAL "package-last")
            set(_PACKAGE_INSTALL_PREFIX_PRE ${CMAKE_INSTALL_PREFIX})
            set(_PACKAGE_INSTALL_PREFIX_POST ${PACKAGE_INSTALL_DIR})
        elseif( ${PACKAGE_INSTALL_SCHEME} STREQUAL "package-first")
            set(_PACKAGE_INSTALL_PREFIX_PRE ${CMAKE_INSTALL_PREFIX}/${PACKAGE_INSTALL_DIR})
            set(_PACKAGE_INSTALL_PREFIX_POST )
        else()
            msg_error("INSTALL_SCHEME ${PACKAGE_INSTALL_SCHEME} not defined")
        endif()
        set(PACKAGE_INSTALL_PREFIX_PLAIN ${CMAKE_INSTALL_PREFIX}/${PACKAGE_INSTALL_DIR})

        get_package_install_paths(
            API PACKAGE_INSTALL_INC_PREFIX
            LIB PACKAGE_INSTALL_LIB_PREFIX
            BIN PACKAGE_INSTALL_BIN_PREFIX
        )

        # RPATH for install tree
        set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE) # auto-add external libs paths

        # handle INSTALL_RPATH
        get_package_install_prefix(PACKAGE_INSTALL_LIB_PREFIX "lib")
        list(APPEND CMAKE_INSTALL_RPATH ${PACKAGE_INSTALL_LIB_PREFIX})
        msg_yellow("${PACKAGE_NAME} CMAKE_INSTALL_RPATH:")
        message("${CMAKE_INSTALL_RPATH}")

        msg_green("CMAKE_INSTALL_PREFIX (install): ${CMAKE_INSTALL_PREFIX}")
        message("PACKAGE_INSTALL_SCHEME (include,lib,bin): ${PACKAGE_INSTALL_SCHEME}")
        message("INSTALL_API_PREFIX: ${PACKAGE_INSTALL_INC_PREFIX}")
        message("INSTALL_LIB_PREFIX: ${PACKAGE_INSTALL_LIB_PREFIX}")
        message("INSTALL_BIN_PREFIX: ${PACKAGE_INSTALL_BIN_PREFIX}")
        message("INSTALL_AUX_PREFIX: ${PACKAGE_INSTALL_PREFIX_PLAIN}")

        if(PACKAGE_FILES)
            msg_yellow("\n${name} package, files to install: ${PACKAGE_FILES}")
            install(FILES ${PACKAGE_FILES}
                DESTINATION ${PACKAGE_INSTALL_PREFIX_PLAIN}
            )
        endif()
    else()
        set(PACKAGE_NOEXPORT TRUE)
        msg_green("${PACKAGE_NAME} - NO INSTALL")
    endif()

    msg_green("CMAKE_PREFIX_PATH (package search): ${CMAKE_PREFIX_PATH}")

    set(NAMESPACE_NAME ${PACKAGE_NAME}::)

    set(PACKAGE_WITH_SUBMODULES ${PACKAGE_SUBMODULES})

    # handle NOEXPORT case
    if (NOT PACKAGE_NOEXPORT)
        set(EXPORT_NAME ${PACKAGE_NAME}-export)
        set(EXPORT_FILE ${PACKAGE_NAME}-config.cmake)
        if (NOT PACKAGE_SUBMODULES)
            _install_export()
        endif()
    endif()

    # set default sub-directories

    if (NOT PACKAGE_API_DIR)
        set(PACKAGE_API_DIR ".")
    endif()
    # Default public API subdirectory
    set(API_DIR ${PACKAGE_API_DIR})

    if (NOT PACKAGE_SRC_DIR)
        set(PACKAGE_SRC_DIR ".")
    endif()
    # Default Sources subdirectory
    set(SRC_DIR ${PACKAGE_SRC_DIR})
endmacro()

macro(define_package_submodule name)
    # parse arguments
    set(args_options NOEXPORT)
    set(args_single)
    set(args_multi FILES)

    cmake_parse_arguments("PACKAGE_SUBMODULE" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})

    if(PACKAGE_SUBMODULE_UNPARSED)
        message("Unparsed arguments for define_package_submodule(${name}: ${PACKAGE_SUBMODULE_UNPARSED}")
    endif()

    msg_green("\ndefine_package_submodule ${name}")

    if (NOT PACKAGE_NAME OR NOT _PACKAGE_INSTALL_PREFIX_PRE)
        msg_error("No current package set. Previous call to define_package() missing?")
    endif()

    if (NOT PACKAGE_WITH_SUBMODULES)
        msg_error("Current package: ${PACKAGE_NAME} was not configured with SUBMODULES")
    endif()

    set(SUBMODULE_NAME ${name})
    set(SUBMODULE_NAMESPACE_NAME ${SUBMODULE_NAME}::)

    # custom target for sub-targets aggregation
    add_custom_target(${SUBMODULE_NAME})

    msg_blue("PACKAGE: ${PACKAGE_NAME} SUBMODULE: ${SUBMODULE_NAME}")

    if ( ${PACKAGE_INSTALL_SCHEME} STREQUAL "package-last" )
        set(_PACKAGE_INSTALL_PREFIX_POST ${_PACKAGE_INSTALL_PREFIX_POST}/${SUBMODULE_NAME})
    elseif( ${PACKAGE_INSTALL_SCHEME} STREQUAL "package-first" )
        set(_PACKAGE_INSTALL_PREFIX_PRE ${_PACKAGE_INSTALL_PREFIX_PRE}/${SUBMODULE_NAME})
    else()
        msg_error("INSTALL_SCHEME ${PACKAGE_INSTALL_SCHEME} not defined")
    endif()
    set(PACKAGE_INSTALL_PREFIX_PLAIN ${PACKAGE_INSTALL_PREFIX_PLAIN}/${SUBMODULE_NAME})

    get_package_install_paths(
        API PACKAGE_INSTALL_INC_PREFIX
        LIB PACKAGE_INSTALL_LIB_PREFIX
        BIN PACKAGE_INSTALL_BIN_PREFIX
    )

    message("INSTALL_API_PREFIX: ${PACKAGE_INSTALL_INC_PREFIX}")
    message("INSTALL_LIB_PREFIX: ${PACKAGE_INSTALL_LIB_PREFIX}")
    message("INSTALL_BIN_PREFIX: ${PACKAGE_INSTALL_BIN_PREFIX}")
    message("INSTALL_AUX_PREFIX: ${PACKAGE_INSTALL_PREFIX_PLAIN}")

    msg_yellow("full submodule namespace: ${NAMESPACE_NAME}${SUBMODULE_NAMESPACE_NAME}")
    message("short submodule namespace: ${SUBMODULE_NAMESPACE_NAME}")

    # set INSTALL_RPATH
    get_global_var(PACKAGE_INSTALL_RPATH _PACKAGE_INSTALL_RPATH)
    list(APPEND _PACKAGE_INSTALL_RPATH ${PACKAGE_INSTALL_LIB_PREFIX})

    list(APPEND CMAKE_INSTALL_RPATH ${_PACKAGE_INSTALL_RPATH})
    msg_yellow("${SUBMODULE_NAME} CMAKE_INSTALL_RPATH:")
    message("${CMAKE_INSTALL_RPATH}")

    set_global_var(PACKAGE_INSTALL_RPATH _PACKAGE_INSTALL_RPATH)

    # handle NOEXPORT case
    if (NOT PACKAGE_SUBMODULE_NOEXPORT)
        set(EXPORT_NAME ${PACKAGE_NAME}-${SUBMODULE_NAME}-export)
        set(EXPORT_FILE ${PACKAGE_NAME}-${SUBMODULE_NAME}-config.cmake)
        msg_yellow("export name: ${EXPORT_NAME}")
        message("export file: ${EXPORT_FILE}")
        _install_export()
    endif()

    if(PACKAGE_SUBMODULE_FILES)
        msg_yellow("\npackage ${PACKAGE_NAME} module ${SUBMODULE_NAME}, files to install: ${PACKAGE_SUBMODULE_FILES}")
        install(FILES ${PACKAGE_SUBMODULE_FILES}
            DESTINATION ${PACKAGE_INSTALL_PREFIX_PLAIN}
        )
    endif()

endmacro() # define_package_submodule

macro(use_package _package_name)
    # parse arguments
    set(args_options NOT_REQUIRED)
    set(args_single SUBMODULE)
    set(args_multi)

    cmake_parse_arguments("USE" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})

    if(USE_UNPARSED)
        msg_err("Unparsed arguments for use_package(${_package_name}: ${USE_UNPARSED}")
    endif()

    if(NOT PACKAGE_NAME)
        msg_err("use_package() called before a package is defined")
    endif()

    msg_green("use package: ${_package_name} ${USE_SUBMODULE}")
    if( (NOT SINGLE_TREE) OR ( NOT ${_package_name} IN_LIST SINGLE_TREE_MODULES) )
        if(NOT "${_package_name}" STREQUAL "${PACKAGE_NAME}")
            if (USE_SUBMODULE)
                set(_USED_PACKAGE_NAME ${_package_name}-${USE_SUBMODULE})
                set(_FIND_PACKAGE_NAMES ${_package_name} ${_USED_PACKAGE_NAME})
            else()
                set(_USED_PACKAGE_NAME ${_package_name})
                set(_FIND_PACKAGE_NAMES ${_package_name})
            endif()
            if(NOT ${_USED_PACKAGE_NAME}_FOUND)
		    message("find_package:${_USED_PACKAGE_NAME}, called from package:${PACKAGE_NAME} NAMES:${_FIND_PACKAGE_NAMES} NOT_REQUIRED:${USE_NOT_REQUIRED}")
            if (NOT ${USE_NOT_REQUIRED})
                find_package(${_USED_PACKAGE_NAME} CONFIG REQUIRED
                    NAMES ${_FIND_PACKAGE_NAMES}
                    CONFIGS ${_USED_PACKAGE_NAME}-config.cmake)
            else()
                find_package(${_USED_PACKAGE_NAME} CONFIG
                    NAMES ${_FIND_PACKAGE_NAMES}
                    CONFIGS ${_USED_PACKAGE_NAME}-config.cmake)    
            endif()                
                if (${${_USED_PACKAGE_NAME}_FOUND})
                    msg_blue("${_USED_PACKAGE_NAME} found")
                endif()
            else()
                msg_blue("find_package:${_USED_PACKAGE_NAME} skipped - already included")
            endif()
        else ()
            msg_blue("find_package( ${ARGV} ) skipped - inside package: ${PACKAGE_NAME}")
        endif()
    else()
        msg_blue("find_package( ${ARGV} ) skipped - single tree setup")
    endif()
endmacro() # use_package

set_parent_var(_PACKAGES_CMAKE_INCLUDED YES)
endif()

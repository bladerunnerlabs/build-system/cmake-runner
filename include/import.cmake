# import.cmake
#
# Copyright (c) 2019-2021 - BladeRunner Labs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

if (NOT _MESSAGES_CMAKE_INCLUDED)
    include(${INC_CMAKE_DIR}/messages.cmake)
endif()

if (NOT _IMPORT_CMAKE_INCLUDED)

include(GNUInstallDirs)
include(FindPackageHandleStandardArgs)

macro(project_import_hints)
    # parse arguments
    set(args_options )
    set(args_single )
    set(args_multi PATH LIB_PATH INC_PATH)
    cmake_parse_arguments("IMPORT" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})

    if (IMPORT_PATH)
        list(APPEND CMAKE_PREFIX_PATH ${IMPORT_PATH})
        list(APPEND PROJ_LIB_PATH_HINTS ${IMPORT_PATH})
        list(APPEND PROJ_INC_PATH_HINTS ${IMPORT_PATH})
    endif()

    if (IMPORT_LIB_PATH)
        list(APPEND PROJ_LIB_PATH_HINTS ${IMPORT_LIB_PATH})
    endif()

    if (IMPORT_INC_PATH)
        list(APPEND PROJ_INC_PATH_HINTS ${IMPORT_INC_PATH})
    endif()

    msg_green("set import path hints")
    message("libs import: ${PROJ_LIB_PATH_HINTS}")
    message("headers import: ${PROJ_INC_PATH_HINTS}")
    message("CMAKE_PREFIX_PATH (package search): ${CMAKE_PREFIX_PATH}")
endmacro()

macro(import_lib lib_name)
    # parse arguments
    set(args_options CUSTOM)
    set(args_single )
    set(args_multi EXTRA_NAMES PATH_HINTS PATH_SUFFIXES API_HEADERS)
    cmake_parse_arguments("LIB" "${args_options}" "${args_single}" "${args_multi}" ${ARGN})

    # generate lists of possible suffixes
    list(APPEND LIB_PATH_SUFFIXES_LIB
            "${lib_name}")

    if (LIB_PATH_SUFFIXES)
        list(TRANSFORM LIB_PATH_SUFFIXES
                APPEND "/${lib_name}"
                OUTPUT_VARIABLE SUFFIXES_LIB_NAME)
        list(APPEND LIB_PATH_SUFFIXES
                ${SUFFIXES_LIB_NAME})
    endif()
    list(APPEND LIB_PATH_SUFFIXES
            "${lib_name}")

    list(TRANSFORM LIB_PATH_SUFFIXES
            APPEND "/include"
            OUTPUT_VARIABLE SUFFIXES_INCLUDE_AFTER)
    list(TRANSFORM LIB_PATH_SUFFIXES
            PREPEND "include/"
            OUTPUT_VARIABLE SUFFIXES_INCLUDE_BEFORE)
    list(TRANSFORM LIB_PATH_SUFFIXES
            APPEND "/lib"
            OUTPUT_VARIABLE SUFFIXES_LIB_AFTER)
    list(TRANSFORM LIB_PATH_SUFFIXES
            PREPEND "lib/"
            OUTPUT_VARIABLE SUFFIXES_LIB_BEFORE)

    # find the library
    set(LIB_PATH_SUFFIXES_LIB ${LIB_PATH_SUFFIXES})
    list(APPEND LIB_PATH_SUFFIXES_LIB
            ${SUFFIXES_LIB_BEFORE}
            ${SUFFIXES_LIB_AFTER}
            "lib")
    list(REMOVE_DUPLICATES LIB_PATH_SUFFIXES_LIB)

    if (LIB_CUSTOM)
        set(CUSTOM_FIND_OPTS "NO_CMAKE_PATH NO_CMAKE_SYSTEM_PATH")
    endif()

    unset(${lib_name}_LIBRARY CACHE)
    find_library(
            ${lib_name}_LIBRARY # variable to store the results
            NAMES ${lib_name} ${LIB_EXTRA_NAMES}
            NAMES_PER_DIR
            REQUIRED
            ${CUSTOM_FIND_OPTS}
            HINTS ${LIB_PATH_HINTS} ${PROJ_LIB_PATH_HINTS}
            PATH_SUFFIXES ${LIB_PATH_SUFFIXES_LIB})
    if (${lib_name}_LIBRARY-NOTFOUND)
        msg_error("lib ${lib_name} ${LIB_EXTRA_NAMES} not found")
    endif()

    # find the headers
    set(LIB_PATH_SUFFIXES_INC ${LIB_PATH_SUFFIXES})
    list(APPEND LIB_PATH_SUFFIXES_INC
            ${SUFFIXES_INCLUDE_BEFORE}
            ${SUFFIXES_INCLUDE_AFTER}
            "include")
    list(REMOVE_DUPLICATES LIB_PATH_SUFFIXES_INC)

    if (NOT LIB_API_HEADERS)
        set(LIB_API_HEADERS "${lib_name}.h")
    endif()

    unset(${lib_name}_INCLUDE_DIR CACHE)
    find_path(${lib_name}_INCLUDE_DIR # variable to store the results
            NAMES ${LIB_API_HEADERS}
            REQUIRED
            ${CUSTOM_FIND_OPTS}
            HINTS ${LIB_PATH_HINTS} ${PROJ_INC_PATH_HINTS} ${CMAKE_INSTALL_INCLUDEDIR}
            PATH_SUFFIXES ${LIB_PATH_SUFFIXES_INC})
    if (${lib_name}_LIBRARY-NOTFOUND)
        msg_error("${lib_name} headers ${LIB_API_HEADERS} not found")
    endif()

    msg_green("Import lib: ${lib_name}")
    message("${lib_name} lib: ${${lib_name}_LIBRARY}")
    message("${lib_name} include: ${${lib_name}_INCLUDE_DIR}")

    # handle find_package
    #find_package_handle_standard_args(${lib_name} DEFAULT_MSG
    #        ${lib_name}_LIBRARY
    #        ${lib_name}_INCLUDE_DIR)

    mark_as_advanced(${lib_name}_LIBRARY ${lib_name}_INCLUDE_DIR)

    #if(${lib_name}_FOUND AND NOT TARGET ${lib_name}::${lib_name})
    if(NOT TARGET ${lib_name}::${lib_name})
        add_library(${lib_name}::${lib_name} SHARED IMPORTED)
        set_target_properties(
                ${lib_name}::${lib_name}
                PROPERTIES
                INTERFACE_INCLUDE_DIRECTORIES ${${lib_name}_INCLUDE_DIR}
                IMPORTED_LOCATION ${${lib_name}_LIBRARY})
        message("${lib_name} target: ${lib_name}::${lib_name}")
    endif()

endmacro() # import_lib

macro(find_exe VAR EXE_NAME)
	find_path(_DIR ${EXE_NAME})
	if (_DIR)
		set(${VAR} ${_DIR}/${EXE_NAME})
		message("found ${EXE_NAME}: ${${VAR}}")
	else()
		msg_error("${EXE_NAME} not found")
	endif()
endmacro()

set_parent_var(_IMPORT_CMAKE_INCLUDED YES)
endif()

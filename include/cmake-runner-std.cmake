# compiler.cmake
#
# Copyright (c) 2019-2021 - BladeRunner Labs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

if (NOT _CMAKE_RUNNER_STD_INCLUDED)

if (NOT CMAKE_RUNNER_DIR)
    message(FATAL_ERROR "CMAKE_RUNNER_DIR undefined")
endif()

set(INC_CMAKE_DIR ${CMAKE_RUNNER_DIR}/include)

# include CMake utils
include(${INC_CMAKE_DIR}/variables.cmake)
include(${INC_CMAKE_DIR}/messages.cmake)

msg_blue("\nPROJECT: ${CMAKE_PROJECT_NAME}")

include(${INC_CMAKE_DIR}/versions.cmake)
include(${INC_CMAKE_DIR}/options.cmake)
include(${INC_CMAKE_DIR}/packages.cmake)
include(${INC_CMAKE_DIR}/targets.cmake)
include(${INC_CMAKE_DIR}/import.cmake)
#include(${INC_CMAKE_DIR}/compiler.cmake)
include(${INC_CMAKE_DIR}/linker.cmake)
include(${INC_CMAKE_DIR}/conan.cmake)

set_parent_var(_CMAKE_RUNNER_STD_INCLUDED YES)
endif()
